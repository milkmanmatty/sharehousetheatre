﻿using System;
using System.Text;
using System.Web.Mvc;
using umbraco;
using Umbraco.Core.Logging;
using Umbraco.Web.Mvc;
using ShareHouseTheatre.Models;
using System.Text.RegularExpressions;

namespace ShareHouseTheatre.Controllers
{
	public class ContactSurfaceController : SurfaceController
	{

		[HttpPost]
		public ActionResult Contact(ContactModel model)
		{

			ActionResult actionResult;

			//Check for honeypot entry
			if(!String.IsNullOrEmpty(model.Required) || !String.IsNullOrEmpty(model.RequiredToPost)){
				actionResult = base.PartialView("contact/ContactFormSuccess"); //thanks, your spam has been disregarded!
			} else {

				//check for empty required fields
				if(!base.ModelState.IsValid || String.IsNullOrEmpty(model.Name) || String.IsNullOrEmpty(model.Message) || String.IsNullOrEmpty(model.Email) ){
					if(String.IsNullOrEmpty(model.Name)){
						ViewBag.error = "Name cannot be empty";
					} else if (String.IsNullOrEmpty(model.Email)){
						ViewBag.error = "Email cannot be empty";
					} else if (String.IsNullOrEmpty(model.Message)){
						ViewBag.error = "Message cannot be empty";
					} else if (String.IsNullOrEmpty(model.T)){
						ViewBag.error = "Ensure all fields are filled out";
					} else if(!base.ModelState.IsValid){
						ViewBag.error = "Ensure all fields are filled out";
					}
					return base.PartialView("Contact/ContactFormError");
				}

				//Check if the message contains any urls
				//Do this after null checking model.Message
				if(checkForUrls(model.Message)){
					return base.PartialView("contact/ContactFormSuccess"); //thanks, your spam has been disregarded!
				}


				//Do some simple decryption
				//Do this after null checking model.T
				if(decyrptIsValid(model.T) == false) {
					return base.PartialView("contact/ContactFormSuccess"); //thanks, your spam has been disregarded!
				}


				string to = "sharehousetheatreco@gmail.com";
				//string to = "matt@milkmanmatty.com";
				string from = "mailbot@milkmanmatty.com";

				var sb = new StringBuilder();
				sb.AppendFormat("<p>Name: {0}</p>", model.Name);
				sb.AppendFormat("<p>Email: {0}</p>", model.Email);
				if(!String.IsNullOrEmpty(model.Website)){
					sb.AppendFormat("<p>Website: {0}</p>", model.Website);
				}
				sb.AppendFormat("<p>Message: {0}</p>", model.Message);

				try {
					library.SendMail(from, to, "New website enquiry!", sb.ToString(), true);
					actionResult = base.PartialView("Contact/ContactFormSuccess");
				}
				catch (Exception ex) {
					LogHelper.Error(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "msg", ex);
					actionResult = base.PartialView("Contact/ContactFormError");
				}
			}
			return actionResult;
		}

		private bool checkForUrls(string msg) {
			Regex rgx = new Regex(@"((http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?)");
			return rgx.IsMatch(msg);
		}


		private bool decyrptIsValid(string timestamp) {

			var _base = timestamp.ToCharArray();
			char[] salt = "Milkman Matty".ToCharArray();
			char[] dencrypted = new char[_base.Length];
			for(int i = 0; i < _base.Length; i++) {
				dencrypted[i] = (char) ( _base[i] - salt[i%salt.Length] );
			}

			// something like "20180225121840"
			string de_string = new string(dencrypted);
			
			string cur_time = DateTime.Now.ToString("yyyyMMddHHmmss");

			if(long.Parse(de_string)+4 > long.Parse(cur_time)) {
				return false;
			}
			return true;
		}
	}
}