﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShareHouseTheatre.Models
{
	public class ContactModel {
		public string Name { get; set; }
		public string Email { get; set; }
		public string Website { get; set; }
		public string Message { get; set; }
		public string Required { get; set; }
		public string RequiredToPost { get; set; }
		public string T { get; set; }
	}
}